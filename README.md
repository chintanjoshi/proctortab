# proctor-track

XBlock to show ProctorTrack site in Iframe

```
NOTE: Tested for ironwood only
```

## Installation

To Install this XBlock in your platform please follow the commands after activating `edxapp` environment.

```
pip install -e git+https://gitlab.com/chintanjoshi/proctortab@master#egg=proctortab
```

Once done, please enter following Flags in your *.env.json files 

```
PROCTOR_TRACK_TAB_ENABLED = True
PROCTOR_TRACK_TAB_SITE_URL = 'https://proctortrack.com'
```

After that please add following piece of code to `edx-platform/lms/urls.py`

```
if settings.PROCTOR_TRACK_TAB_ENABLED:
    from proctor_tab.views import ProctorTrackTabView

    urlpatterns +=[
        url(
            r'^courses/{}/proctor_track$'.format(
            settings.COURSE_ID_PATTERN,
        ),
        ProctorTrackTabView.as_view(),
        name='proctor_tab',
    ),
    ]

```

After that you can restart the platform and bob's your uncle.

### Enabling

To enable this, in studio go to `Settings -> Advance Settings -> Advance Module List`  and add 

`proctor_tab`