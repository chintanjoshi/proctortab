"""
Xblock that renders a tab in course which loads a url in Iframe

Author: Chintan Joshi
"""

from django.utils.translation import ugettext as _

from xmodule.tabs import CourseTab

from xblock.core import XBlock


class ProctorTabXblock(XBlock):
    """
    Dummy
    """
    pass

class ProctorTab(CourseTab):
    """A new course tab."""
    type = "proctor_tab"
    name = "proctor_tab"
    title = "ProctorTrack"
    view_name = "proctor_tab"
    is_default = True
 
    @classmethod
    def is_enabled(cls, course, user=None):
        """Returns true if this tab is enabled."""

        return 'proctor_tab' in course.advanced_modules

    @classmethod
    def validate(cls, tab_dict, raise_error=True):

        return True
