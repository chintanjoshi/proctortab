"""
Views file of the ProctorTab

Author: Chintan Joshi
"""

import os

from django.conf import settings

from edxmako.shortcuts import render_to_response
from edxmako.paths import add_lookup, lookup_template

from rest_framework.generics import GenericAPIView

from opaque_keys.edx.keys import CourseKey

from courseware.courses import get_course_with_access



class ProctorTrackTabView(GenericAPIView):

    def get(self, request, course_id):
        course_key = CourseKey.from_string(course_id)
        course = get_course_with_access(request.user, "load", course_key)
        add_lookup('main', os.path.dirname(__file__))
        context = {
            'course': course,
            'site_url': settings.PROCTOR_TRACK_TAB_SITE_URL
        }
        return render_to_response("/templates/tab_view.html", context, "main")