"""Setup for proctortab XBlock."""

from __future__ import absolute_import

import os

from setuptools import find_packages, setup

import proctor_tab


def package_data(pkg, root_list):
    """Generic function to find package_data for `pkg` under `root`."""
    data = []
    for root in root_list:
        for dirname, _, files in os.walk(os.path.join(pkg, root)):
            for fname in files:
                data.append(os.path.relpath(os.path.join(dirname, fname), pkg))

    return {pkg: data}

setup(
    name='proctortab',
    version=proctor_tab.__version__,
    description='proctortab Adds a tab in courseware which opens a site in a tab',
    license='Affero GNU General Public License v3 (GPLv3)',
    url="https://gitlab.com/chintanjoshi/proctortab/",
    author="Chintan Joshi",
    author_email="chintanjoshi1995@gmail.com",
    zip_safe=False,
    packages=["proctor_tab"],
    include_package_data=True,
    install_requires=[
        'XBlock',
        'xblock-utils',
    ],
    entry_points={
        'xblock.v1': [
            'proctor_tab = proctor_tab.tabs:ProctorTabXblock',
        ],
        'openedx.course_tab': [
            "proctor_tab = proctor_tab.tabs:ProctorTab",
        ]
    },
    package_data=package_data("proctor_tab", ["static", "templates"]),
)